# oh nice theme now it's OURS

script to easily share configuration files

## Install a theme

To install a theme, just run `apply_theme.py` with python3 in the theme directory, then run `exec bash` to restart your bash


## Create a theme

1. Create a theme directory with this structure:
```
theme_name/theme_descriptor.json
theme_name/(files)
```

where (files) are all the configuration files you wish to include in your theme.

2. Edit theme_descriptor.json to tell the script and the user what to do

it should look like this (everything in caps should be replaced):
```
{
	"name": "THEME NAME",
	"author": "YOUR NAME",

	"notes": "TEXT DISPLAYED TO THE USER BEFORE THEY INSTALL THE THEME",

	"files": {

		"FILENAME": {"dest": "DESTINATION PATH", "mode":"wRITE OR aPPEND"},
	}
}
```
Here's an [example](https://codeberg.org/LinuxPropaganda/termtheme/src/branch/main/example/theme_descriptor.json)

3. That's it!

