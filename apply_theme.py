from os import system, popen
import json


with open("theme_descriptor.json", 'r') as f:
	descriptor = json.load(f)

items = {}
for src, desc in descriptor["files"].items():
	items[src] = {"dest": desc["dest"].replace("~", popen("echo ~").read().splitlines()[0]), "mode": desc["mode"]}


print("\33[0;37m - Terminal Themer -")

print('\n'+'='*24)
for name, value in descriptor.items():
	if name != "files":
		print(f"\33[0;30;42m{name}:\33[0;37m", descriptor[name])
print('='*24, '\n')


backup = input("Backup files? (Y/n)").lower() == 'y'
print("\33[1;32mOK\33[0m")


if backup:
	system("mkdir backup")
	for s, i in items.items():
		system(f"cp {i['dest']} backup/{s}")

	with open("backup/theme_descriptor.json", 'w') as f:

		f.write(json.dumps(

			{
				"author": "automatic",
				"name": "backup",

				"exec bash": descriptor.get("exec bash", 0),

				"files": dict([(src, {"dest": i["dest"], "mode": "w"}) for src, i in items.items()])  # force mode "w" to overwrite
			}

		))

	print("Snapshotted current config. To reload, run this script in backup folder")


print("Applying theme")

for src, desc in items.items():

	with open(src, 'r') as s:
		source = s.read()
	
	with open(desc["dest"], desc.get("mode", "w")) as f:
		f.write(source)

	print("copied", src, "to", desc["dest"])

print("\n -= THEME APPLIED =-")